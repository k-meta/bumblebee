# Base requirements
protobuf==3.20.3
torch==2.0.1
torchvision==0.15.2
torchaudio==2.0.2
transformers[torch,sentencepiece,vision,optuna,sklearn,onnxruntime]==4.30.0
datasets[audio]==2.14.2
huggingface_hub==0.16.4
matplotlib==3.7.2
ipywidgets==8.1.0
# Chapter 2 - Classification
umap-learn==0.5.3
# Chapter 3 - Anatomy
bertviz==1.4.0
# Chapter 4 - NER
seqeval==1.2.2
# Chapter 6 - Summarization
nltk==3.8.1
sacrebleu==2.3.1
rouge-score==0.1.2
py7zr==0.20.5 # Needed for samsum dataset
# Chapter 9 - Few labels
nlpaug==1.1.11
scikit-multilearn==0.2.0
faiss-cpu==1.7.4
# Chapter 10 - Pretraining
psutil==5.9.5
accelerate==0.21.0
# jupyter notebook
notebook==7.0.1
