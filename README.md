# 제목: 허깅페이스 트랜스포머 기반 AI 챗봇 제작
![bummblebee](./images/bumblebee+cat.jpg)
## 교육기간: 4일(일6시간)

## 교육안내:
### 수강대상: 
    메테버스 콘텐츠 제작에 인공지능 기능을 도입에 관심있는 제작자
    인공지능기술에 관심있는 현업인
    모델기반 인공지능을 현업에 도입혀려는 현업인

### 교육과정소개:
### 필요기반지식:
    파이썬 언어에 대한 이해(초, 중급)
    인공지능에 대한 사전 지식없이 수강가능
## 교육내용:
### 1일: - 트랜스포머 소개 및 허깅페이스 운영환경 설정: 트랜스포머 기반 챗봇 0.1 제작
    - 이해: 트랜스포머 및 허깅페이스의 기본 이해 
    - 실습: 허깅페이스 트랜스포머 기반 챗봇 0.1 제작

0. Orientation(소개, 환경설정)
1. Hello Transformers(Featuring DL Basics)
2. Text Classification

### 2일: - 트렌스포머 기반 모델 이해: 트랜스포머 기반 챗봇 0.2 제작
    - 이해: 트랜스포머의 기반이 되는 모델 이해(모델기반 LLM 소개)
    - 실습: 허깅페이스 트랜스포머 기반 챗봇 0.2 제작 다양한 모델 소개 및 적용
3. Transformer Anatomy
4. Multilingual Named Entity Recognition
5. Text Generation

### 3일: - 트랜스포머 요소 기술 이해: 트랜스포머 기반 챗봇 0.3 제작
    - 이해: 트랜스포머의 요소 기술 이해(토크나이저, 임베딩, 어텐션, 디코더, 인코더)
    - 실습: 요소기술 적용한 허깅페이스 트랜스포머 기반 챗봇 0.3 제작
6. Summarization
7. Question Answering
8. Making Transformers Efficient in Production
9. Dealing with Few to No Labels

### 4일: - 트랜스포머 확장 기술 이해: 트랜스포머 기반 챗봇 0.4 제작
    - 이해: 트랜스포머의 확장 기술 이해(다국어, 다모델, 다양한 데이터셋)
    - 실습: 확장기술 적용한 허깅페이스 트랜스포머 기반 챗봇 0.4 제작

10. Training Transformers from Scratch
11. Future Directions

## PC 환경설정 설정
|프로그램               | 다운로드 경로                                                  |
|---------------------|-------------------------------------------------------------|
| 1. windows terminal | [windows store app](https://aka.ms/terminal) 또는 https://github.com/microsoft/terminal |
| 2. git              | https://git-scm.com                                         |
| 3. CUDA             | https://developer.nvidia.com/cuda-downloads                 |
| 4. python           | https://www.python.org/downloads/                           |

## 파이썬 패키지 설치 (윈도우 터미널에서 진행)
```sh
git clone https://gitlab.com/k-meta/bumblebee
cd bumblebee
python -m pip install -r requirements.txt
```

# [참고자료]

## 인공지능의 매운맛!

[![처음시작하는 딥러닝](./images/book-Deep-Learning-From-Scratch.png)](./books/Deep%20Learning%20from%20Scratch%20(Seth%20Weidman,%202019).pdf)

[![심층학습](./images/book-Deep-Learning.png)](./books/Deep%20Learning%20(Ian%20Goodfellow%20et%20al.,%202016).pdf)

[![트랜스포머를 활용한 자연어 처리](./images/book-NLP-with-transformers.png)](./books/NLP%20with%20Transformers%20Building%20Language%20Applications%20with%20Hugging%20Face%20(Tunstall%20et%20al.,%202022).pdf)

## 머신러닝 vs. 자연지능

[![Insights from the brain](./images/book-Insights-from-the-brain.png)](./books/Insights_from_the_brain__The_road_towards_Machine_Intelligence%20(Thiboust,%202021).pdf)

## 바보야 문제는 Attention이야! - Attention is All You Need

[![Attention is All You Need](./images/paper-Attention-is-All-You-Need.png)](./books/Attention%20Is%20All%20You%20Need(Vaswani%20et%20al.,%202017).pdf)

## 스텐포드 파운데이션 모델! ChatGPT를 예언하다.

[![On the Opportunities and Risks ofFoundation Models](./images/book-On%20the%20Opportunities%20and%20Risks%20of%20Foundation%20Models.jpg)](./books/On%20the%20Opportunities%20and%20Risks%20of%20Foundation%20Models(Bommasni%20et%20al.,%202022v3).pdf)
